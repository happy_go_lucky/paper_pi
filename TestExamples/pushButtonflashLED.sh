#!/bin/bash

# a script to toggle GPIO pin for a limited time. 
# when connected to a LED, it will turn the LED on for that amount of time. 

setVars()
{
	# the pin numbers are based on the gpio numbering instead of placement numbering 
	ledPin=25; # this is pin 22 otherwise in placement numbering
	buttonPin=24
	pinModeRead="in";
	pinModeWrite="out";
	minPinNumber=1;
	maxPinNumber=40;
}

unsetVars()
{
	unset ledPin;
	unset pinModeRead;
	unset pinModeWrite;
	unset minPinNumber;
	unset maxPinNumber;
	unset buttonPin;
}

exportPin()
{
	local ledPin=$1;
	echo $ledPin > /sys/class/gpio/export;
}

unexportPin()
{
	local ledPin=$1;
	echo $ledPin > /sys/class/gpio/unexport;
}

isValidSignalValue()
{
	if [ -n $1 ] && [ $1="1" -o $1="0" ];
	then
		return 1;
	else
		return 0;
	fi
}

isValidPinMode()
{
	if [ -n $1 ] && [ $1=$pinModeRead -o $1=$pinModeWrite ];
	then
		return 1;
	else
		return 0;
	fi
}

isValidPinNumber()
{
	if [ -n $1 ] && [ $1 -ge $minPinNumber -a $1 -le $maxPinNumber ];
	then
		return 1;
	else
		return 0;
	fi
}

throwErrorAndExit()
{
	unsetVars;
	unexportPin $ledPin;
	echo $1;
	exit 1;
}

writeValueToPin()
{
	local value=$1;
	local pin=$2;
	isValidPinNumber $pin;
	local result1=$?;
	isValidSignalValue $value;
	local result2=$?;
	
	if [ $result1 -eq 1 ] && [ $result2 -eq 1 ];
	then
		echo "writing \"$value\" to pin $pin";
		echo $value > /sys/class/gpio/gpio$pin/value;
	else
		throwErrorAndExit "unable to write value: \"$value\" to pin $pin";
	fi
}

setPinMode()
{
	local mode=$1;
	local pin=$2;
	isValidPinMode $mode;
	local result1=$?;
	isValidPinNumber $pin;
	local result2=$?;
	
	if [ $result1 -eq 1 ] && [ $result2 -eq 1 ];
	then
		echo "setting the pin $pin to \"$mode\"";
		echo "out" > /sys/class/gpio/gpio$pin/direction;
	else
		throwErrorAndExit "unable to set pin $pin to mode \"$mode\"";
	fi
}

executeInstructions()
{
	unsetVars;
	setVars;
	exportPin $ledPin;
	exportPin $buttonPin;
	sleep 0.5;
	setPinMode $pinModeWrite $ledPin;
	setPinMode $pinMModeRead $buttonPin;
	writeValueToPin 1 $ledPin;
	sleep 5;
	writeValueToPin 0 $ledPin;
	unexportPin $ledPin;
	unsetVars;
}

test()
{
	local pin=2;
	local mode="out";
	local value=1;
	local temp=0;
	isValidPinNumber 1;
	local result1=$?;
	
	if [ $result1 -eq 1 ] && [ $temp -eq 1 ];
	then
		echo "true";
	else
		echo "false";
	fi
	unsetVars;
}

executeInstructions;
#test;
