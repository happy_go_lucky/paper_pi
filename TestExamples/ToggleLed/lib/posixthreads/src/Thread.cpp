#include "../inc/Thread.h"

Thread::Thread(posixthreads::DetachType detachType, int stackSize)
{
	//~ std::cout << "Thread::Thread()" << std::endl;
	
	_detachType = detachType;
	_stackSize = stackSize;
	_isStarted = false;
	
	_initArribs();
	_setDetachType();
	_setStackSize();
	setCancelAttribs(posixthreads::CancelState::CANCELLABLE,
					posixthreads::CancelType::DEFFERRED);
}
		
Thread::~Thread()
{
	//~ std::cout << "Thread::~Thread()" << std::endl;
	
	int state = pthread_attr_destroy(&_attributes);
	if (state != EXIT_SUCCESS)
	{
		std::cout << strerror(state) << std::endl;
	}
	terminateThread();
}

void Thread::start(void* arg, void* returnVal)
{
	if (!_isStarted)
	{
		_isStarted = true;		
		_threadArgs = arg;
		_returnVal = returnVal;
		int state = pthread_create(&_id, NULL, &Thread::_execute, this);
		
		if (state != EXIT_SUCCESS)
		{
			std::cout << strerror(state) << std::endl;
			throw ThreadException((char*) "unable to create the thread");
		}
	}
}

void Thread::join()
{
	int state = pthread_join(_id, &_returnVal);
	if (state != EXIT_SUCCESS)
	{
		std::cout << strerror(state) << std::endl;
		throw ThreadException((char*) "unable to join the thread with main thread");
	}
}

void Thread::requestCancel()
{
	int state = pthread_cancel(_id);
	if (state != EXIT_SUCCESS)
	{
		std::cout << strerror(state) << std::endl;
		throw ThreadException((char*) "unable to cancel the thread");
	}
	
	_isStarted = false;
}

/**
 * This will be the function that this thread will perform
 * the arguments needed for this thread to run will be supplied in start()
 * */
void* Thread::_execute(void* thr)
{
	return reinterpret_cast<Thread* >(thr)->threadRun();
}

/**
 * only allow manual exit for independent threads
 * use force exit for extreme cases.
 * */
void Thread::terminateThread(bool forceExit) const
{
	if (forceExit || _detachType == posixthreads::DetachType::INDEPENDENT)
	{
		pthread_exit(_returnVal);
	}
}

void Thread::_initArribs()
{
	int attirbInitState = pthread_attr_init(&_attributes);
	
	if (attirbInitState != EXIT_SUCCESS)
	{
		std::cout << strerror(attirbInitState) << std::endl;
		throw ThreadException((char*) "unable to initilize attributes for thread");
	}
}

void Thread::_setDetachType()
{
	int detachState;
	
	if (_detachType == posixthreads::DetachType::INDEPENDENT)
	{
		detachState = pthread_attr_setdetachstate(&_attributes, PTHREAD_CREATE_DETACHED);
	}
	else
	{
		detachState = pthread_attr_setdetachstate(&_attributes, PTHREAD_CREATE_JOINABLE);
	}
	
	if (detachState != EXIT_SUCCESS)
	{
		std::cout << strerror(detachState) << std::endl;
		throw ThreadException((char*) "unable to set the detach state for thread");
	}
}

void Thread::_setStackSize()
{
	if (_stackSize > PTHREAD_STACK_MIN)
	{
		int stackSizeState = pthread_attr_setstacksize(&_attributes, _stackSize);
		if (stackSizeState != EXIT_SUCCESS)
		{
			std::cout << strerror(stackSizeState) << std::endl;
			throw ThreadException((char*) "unable to set the stack size for thread");
		}
	}
}

void Thread::setCancelAttribs(posixthreads::CancelState cancelState, 
							posixthreads::CancelType cancelType)
{
	int prevState = 0;
	int prevType = 0;
	pthread_setcancelstate(static_cast<int>(cancelState), &prevState);
	pthread_setcanceltype(static_cast<int>(cancelType), &prevType);
}
