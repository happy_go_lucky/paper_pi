#include "../inc/ConditionVariable.h"

ConditionVariable::ConditionVariable() noexcept
{
	_cv = nullptr;
	_cvAttrib = nullptr;
}

ConditionVariable::~ConditionVariable()
{
	if (_cv != nullptr) delete _cv;
	if (_cvAttrib != nullptr) delete _cvAttrib;
}

void ConditionVariable::initialize(MutexLock* mutex, 
				posixthreads::ProcessAccessType accessType)
{
	_cv = new pthread_cond_t();
	_cvAttrib = new pthread_condattr_t();
	
	pthread_condattr_init(_cvAttrib);
	_setAccessType(_accessType);
	_mutexLock = mutex;
	
	#ifndef NDEBUG
	int state = pthread_cond_init(_cv, NULL);
	if (state != EXIT_SUCCESS)
	{
		std::cout << strerror(state) << std::endl;
		throw ThreadException((char*) "unable to initialize condition variable");
	}
	#else
	pthread_cond_init(_cv, NULL);
	#endif
}

void ConditionVariable::cleanup()
{
	#ifndef NDEBUG
	int stateCV = pthread_cond_destroy(_cv);
	int stateAtrrib = pthread_condattr_destroy(_cvAttrib);
	
	if (stateCV != EXIT_SUCCESS || stateAtrrib != EXIT_SUCCESS)
	{
		std::cout << strerror(stateCV) << std::endl;
		std::cout << strerror(stateAtrrib) << std::endl;
		throw ThreadException((char*) "unable to destroy condition variable");
	}
	#else
	pthread_cond_destroy(_cv);
	pthread_condattr_destroy(_cvAttrib);
	#endif
	
	delete _cv;
	delete _cvAttrib;
	_cv = nullptr;
	_cvAttrib = nullptr;
}

/**
 * unblocks one of the waiting threads on the condition variable
 * */
void ConditionVariable::signal(posixthreads::ThreadEventCondition& threadEventCondition)
{
	_mutexLock->lock();
	threadEventCondition = posixthreads::ThreadEventCondition::RUN;
	#ifndef NDEBUG
	int state = pthread_cond_signal(_cv);
	_mutexLock->unlock();
	if (state != EXIT_SUCCESS)
	{
		std::cout << strerror(state) << std::endl;
		throw ThreadException((char*) "unable to signal the thread waiting on condition variable");
	}
	#else
	pthread_cond_broadcast(_cv);
	_mutexLock->unlock();
	#endif
}

/**
 * if more than one threads are being blocked on the condition variable
 * this will unblock all of them
 * */
void ConditionVariable::signalAll(posixthreads::ThreadEventCondition& threadEventCondition)
{
	_mutexLock->lock();
	threadEventCondition = posixthreads::ThreadEventCondition::RUN;
	#ifndef NDEBUG
	int state = pthread_cond_broadcast(_cv);
	_mutexLock->unlock();
	if (state != EXIT_SUCCESS)
	{
		std::cout << strerror(state) << std::endl;
		throw ThreadException((char*) "unable broadcast the threads waiting on condition variable");
	}
	#else
	pthread_cond_broadcast(_cv);
	_mutexLock->unlock();
	#endif
}

/**
 * The calling thread will wait on the condition, and blocked.
 * It will also unlock the mutex variable specified.
 * The specified mutex variable has to be locked by the caller thread before waiting
 * Once the condition is met, the mutex is returned to the caller thread.
 * */
void ConditionVariable::waitForSignal(posixthreads::ThreadEventCondition& threadEventCondition)
{
	// wait call expects the mutex to be locked before it can wait
	_mutexLock->lock();
	threadEventCondition = posixthreads::ThreadEventCondition::WAIT;
	#ifndef NDEBUG
	int state = EXIT_FAILURE;
	while (threadEventCondition == posixthreads::ThreadEventCondition::WAIT)
	{
		state = pthread_cond_wait(_cv, _mutexLock->_mutex);
	}
	_mutexLock->unlock();
	
	if (state != EXIT_SUCCESS)
	{
		std::cout << strerror(state) << std::endl;
		throw ThreadException((char*) "unable to wait on a condition variable");
	}
	#else
	while (threadEventCondition == posixthreads::ThreadEventCondition::WAIT)
	{
		// the calling thread is put to sleep and the mutex lock is released
		// once the calling thread is woken up, the mutex is locked again
		pthread_cond_wait(_cv, _mutexLock->_mutex);
	}
	// mutex is supposed to be unlocked once the condition is valid and wait is over
	_mutexLock->unlock();
	#endif
}

/**
 * block the calling thread for a given time and release the mutex
 * also stops the wait if signal or broadcast is recieved
 * */
void ConditionVariable::timedWait(int waitTimeInSeconds)
{
	struct timespec time;
	helperfxns::getCurrentTimeSpec(time);
	// also look into the following if above doesn't work
	// clock_gettime(CLOCK_MONOTONIC, &time);
	time.tv_sec += waitTimeInSeconds;
	int state = EXIT_FAILURE;
	
	_mutexLock->lock();
	#ifndef NDEBUG
	state = pthread_cond_timedwait(_cv, _mutexLock->_mutex, &time);
	_mutexLock->unlock();
	if (state != EXIT_SUCCESS)
	{
		std::cout << strerror(state) << std::endl;
		throw ThreadException((char*) "unable to set timed wait on condition variable");
	}
	#else
	pthread_cond_timedwait(_cv, _mutexLock->_mutex, &time);
	_mutexLock->unlock();
	#endif
}

/**
 * returns access type
 * */
posixthreads::ProcessAccessType ConditionVariable::getAccessType() const noexcept
{
	int accessType = -1;
	int retVal = pthread_condattr_getpshared(_cvAttrib, &accessType);
	
	if (retVal == EXIT_SUCCESS)
	{
		if (accessType == PTHREAD_PROCESS_SHARED)
			return posixthreads::ProcessAccessType::SHARED;
		else
			return posixthreads::ProcessAccessType::PRIVATE;
	}
	else
	{
		return posixthreads::ProcessAccessType::INVALID;
	}
}

void ConditionVariable::_setAccessType(posixthreads::ProcessAccessType accessType)
{
	if (accessType == posixthreads::ProcessAccessType::SHARED)
		pthread_condattr_setpshared(_cvAttrib, PTHREAD_PROCESS_SHARED);
	else
		pthread_condattr_setpshared(_cvAttrib, PTHREAD_PROCESS_PRIVATE);
}
