ver = c++11

obj_dir = obj
src_dir = src
inc_dir = include
dep_dir = deps
sharedlib_name = libposixthreads.so
sharedlib_path = ../
debug_flag = -D
release_build = rr
debug_build = dd
final_build = ff

# position independent code
shared_lib_flag = -fPIC

#specify all the include directories here
vpath %.cpp $(src_dir)
vpath %.h $(inc_dir)
vpath %.d $(dep_dir)
vpath %.o $(obj_dir)

#0-3 >
optimize_lvl_d = 0
optimize_lvl_r = 2
optimize_lvl_f = 3

build_type = -O$(optimize_lvl_r)

ifeq ($(comm), $(release_build))
	build_type = -O$(optimize_lvl_r)
else ifeq ($(comm), $(final_build))
	build_type = -O$(optimize_lvl_f)
else
	build_type = -O$(optimize_lvl_d)
	debug_flag = -D NDEBUG
endif

#0: no info, 3 max info.
debug_lvl = 3

c_flags = $(build_type) -pedantic -Wpedantic -Wall \
	-Wextra -pthread -std=$(ver) -g$(debug_lvl) -ggdb$(debug_lvl) \
	$(debug_flag) $(shared_lib)

l_flags = -Wl,-soname,$(sharedlib_name)

#-MF $(dep_dir)/$*.d: set the dependency file to depname.d
#MT is target
#MF is the filename where the dependencies are saved to
gen_deps = -MT $(@:.d=.o) -MM -MF $(dep_dir)/$*.d

lib_files = ReadWriteLock.cpp\
	SemaphoreUnnamed.cpp\
	SemaphoreNamed.cpp\
	SharedMemory.cpp\
	Thread.cpp\
	ConditionVariable.cpp\
	MutexLock.cpp\
	ThreadException.cpp

archive_files = $(lib_files:.cpp=.o)
ar_dep_files = $(lib_files:.cpp=.d)
archive_file_loc = $(addprefix $(obj_dir)/,$(archive_files))

all: $(sharedlib_name)

# by default gcc will produce an executable, but by using -shared
# flag, it will indicate gcc to produce a shared library file instead
$(sharedlib_name): $(archive_files)
	$(CXX) -shared $(l_flags) $(archive_file_loc) -o $(sharedlib_path)$@

%.o: %.cpp
	$(CXX) $(shared_lib_flag) $(c_flags) -c $< -o $(obj_dir)/$@

# run setup before running the rule
%.d: %.cpp | setup
	$(CXX) $(c_flags) $< $(gen_deps)

#include must not precede the default target
include $(ar_dep_files)

.PHONY: clean
.PHONY: all
.PHONY: setup
.PHONY: debug

clean:
	@rm -f $(obj_dir)/*.o
	@rm -f *.o
	@rm -f $(dep_dir)/*.d
	@rm -f *.d
	@rm -f $(sharedlib_path)$(sharedlib_name)
	@echo cleaned all temporary files

setup:
	@mkdir -p $(obj_dir)
	@mkdir -p $(dep_dir)

debug:
	nm $(archive_files)
