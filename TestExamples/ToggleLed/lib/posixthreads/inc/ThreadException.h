#ifndef THREAD_EXCEPTION_H
#define THREAD_EXCEPTION_H

#include <exception>

using std::exception;

class ThreadException : public exception
{
	public:
		ThreadException( char* = (char*) "Exception!" );
		virtual const char* what() const throw();
		
	private:
		char* _message;
};

#endif
