#ifndef SEMAPHORE_UNNAMED_H
#define SEMAPHORE_UNNAMED_H

#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>
#include "SharedMemory.h"
#include "SemaphoreDefs.h"

class SemaphoreUnnamed
{
	public:
		SemaphoreUnnamed(SemaphoreDefs::SharingType sharingType);
		~SemaphoreUnnamed();
		
		SemaphoreUnnamed() = delete;
		SemaphoreUnnamed(const SemaphoreUnnamed& sem) = delete;
		SemaphoreUnnamed(SemaphoreUnnamed&& sem) = delete;
		SemaphoreUnnamed& operator =(const SemaphoreUnnamed& sem) = delete;
		SemaphoreUnnamed& operator =(SemaphoreUnnamed&& sem) = delete;
		
		void initThreadSharing(int semaphoreCount);
		void initProcessSharing(int semaphoreCount);
		void wait();
		void post();
		void timedWait(int waitTimeInSeconds);
		void cleanup();

	private:
		sem_t* _semaphore;
		int _semaphoreCount;
		SemaphoreDefs::SharingType _sharingType;
		SharedMemory* _sharedMemory;
};

#endif
