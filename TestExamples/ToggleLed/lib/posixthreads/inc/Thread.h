#ifndef THREAD_H
#define THREAD_H

/**
 * This class is an oop wrapper around pthreads.
 * 
 * */

#include <iostream>
#include <pthread.h>
#include <limits.h>
#include <string.h>
#include "ThreadException.h"
#include "ThreadDefs.h"

class Thread
{
	public:
		Thread(posixthreads::DetachType detachType, int stackSize = PTHREAD_STACK_MIN);
		Thread(const Thread& thread) = delete;
		Thread(Thread&& thread) = delete;
		
		virtual ~Thread();
		
		Thread& operator= (const Thread& thread) = delete;
		Thread& operator= (Thread&& thread) = delete;
		
		inline unsigned int getId() const noexcept
		{
			return _id;
		}
		
		inline posixthreads::DetachType getDetachType() const noexcept
		{
			return _detachType;
		}
		
		void setCancelAttribs(posixthreads::CancelState cancelState, 
							posixthreads::CancelType cancelType);
		void start(void* arg = nullptr, void* returnVal = nullptr);
		void join();
		virtual void* threadRun() = 0;
		void requestCancel();
		void terminateThread(bool forceExit = false) const;
		
	protected:
		// this function will be used as the global function for pthread_create
		static void* _execute(void* thr);	// should call the run()
		void* _threadArgs;
		void* _returnVal;
		
	private:
		pthread_t _id;
		pthread_attr_t _attributes;
		posixthreads::DetachType _detachType;
		posixthreads::CancelState _cancelState;
		int _stackSize;
		bool _isStarted;
		
		void _initArribs();
		void _setDetachType();
		void _setStackSize();
};

#endif
