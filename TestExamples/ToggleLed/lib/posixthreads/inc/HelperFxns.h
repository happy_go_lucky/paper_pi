#ifndef HELPER_FUNCTIONS_H
#define HELPER_FUNCTIONS_H

#include <iostream>
#include <ctime>
#include <cstdarg>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <bitset>

using namespace std;

namespace helperfxns
{
	enum AccessPermissions
	{
		NONE = 0,
		R,
		W,
		X,
		RW,
		RX,
		WX,
		WRX // WRX
	};
	
	enum PermissionCategory
	{
		OWNER = 0,
		GROUP,
		OTHERS
	};
	
	inline long int secondsSinceEpoc()
	{
		return time(nullptr);
	}
	
	inline double getProcessTimeInSeconds()
	{
		return clock() / CLOCKS_PER_SEC;
	}
	
	inline double getTimeDiffInSeconds(clock_t start, clock_t end)
	{
		return difftime(end, start);
	}
	
	inline clock_t getProcessTicks()
	{
		return clock();
	}
	
	inline void getCurrentTimeSpec(timespec& ts)
	{
		timespec_get(&ts, TIME_UTC);
	}
	
	inline void cstrConcat(string& returnStr, int num, ...)
	{
		va_list ptrToArgList;
		
		// point to the start of the argument list followed by first param
		va_start(ptrToArgList, num);
		const char* strArray = new char[num];
		
		for (int ii = 0; ii < num; ii++)
		{
			// get the argument of type from the list
			strArray = va_arg(ptrToArgList, const char*);
			returnStr += strArray[ii];
		}
		
		va_end(ptrToArgList);
	}
	
	inline mode_t getAccessPermissionBits(AccessPermissions ownerPerm,
										AccessPermissions groupPerm,
										AccessPermissions othersPerm)
	{
		AccessPermissions permissions;
		PermissionCategory category;
		mode_t permissionBits = 0;
		
		auto buildPermissions = [&permissions, &category, &permissionBits]() -> mode_t
		{
			switch (permissions)
			{
				case AccessPermissions::R:
					switch(category)
					{
						case PermissionCategory::OWNER:
						return S_IRUSR;
						
						case PermissionCategory::GROUP:
						return S_IRGRP;
						
						case PermissionCategory::OTHERS:
						return S_IROTH;
					}
				break;
				
				case AccessPermissions::W:
					switch(category)
					{
						case PermissionCategory::OWNER:
						return S_IWUSR;
						
						case PermissionCategory::GROUP:
						return S_IWGRP;
						
						case PermissionCategory::OTHERS:
						return S_IWOTH;
					}
				break;
				
				case AccessPermissions::X:
					switch(category)
					{
						case PermissionCategory::OWNER:
						return S_IXUSR;
						
						case PermissionCategory::GROUP:
						return S_IXGRP;
						
						case PermissionCategory::OTHERS:
						return S_IXOTH;
					}
				break;
				
				case AccessPermissions::RW:
					switch(category)
					{
						case PermissionCategory::OWNER:
						return S_IRUSR | S_IWUSR;
						
						case PermissionCategory::GROUP:
						return S_IRGRP | S_IWGRP;
						
						case PermissionCategory::OTHERS:
						return S_IROTH | S_IWOTH;
					}
				break;
				
				case AccessPermissions::RX:
					switch(category)
					{
						case PermissionCategory::OWNER:
						return S_IRUSR | S_IXUSR;
						
						case PermissionCategory::GROUP:
						return S_IRGRP | S_IXGRP;
						
						case PermissionCategory::OTHERS:
						return S_IROTH | S_IXOTH;
					}
				break;
				
				case AccessPermissions::WX:
					switch(category)
					{
						case PermissionCategory::OWNER:
						return S_IWUSR | S_IXUSR;
						
						case PermissionCategory::GROUP:
						return S_IWGRP | S_IXGRP;
						
						case PermissionCategory::OTHERS:
						return S_IWOTH | S_IXOTH;
					}
				break;
				
				case AccessPermissions::WRX:
					switch(category)
					{
						case PermissionCategory::OWNER:
						return S_IRWXU;
						
						case PermissionCategory::GROUP:
						return S_IRWXG;
						
						case PermissionCategory::OTHERS:
						return S_IRWXO;
					}
				break;
				
				case AccessPermissions::NONE:
				default:
				return permissionBits;
			}
			return permissionBits;
		};
		
		permissions = ownerPerm;
		category = PermissionCategory::OWNER;
		mode_t owner = buildPermissions();
		
		permissions = groupPerm;
		category = PermissionCategory::GROUP;
		mode_t group = buildPermissions();
		
		permissions = othersPerm;
		category = PermissionCategory::OTHERS;
		mode_t others = buildPermissions();
		
		return owner | group | others;
	}
	
	inline unsigned long getPageSize()
	{
		return sysconf( _SC_PAGESIZE );
	}
	
	inline void getCurrentDirectiory(char* buffer, size_t size)
	{
		getcwd(buffer, size);
	}
	
	inline void printBits32 (int data)
	{
		cout << bitset<32> (data);
	}
	
	// 1000000000ull. ull is unsigned long long. used for 64 bit numbers
}
#endif
