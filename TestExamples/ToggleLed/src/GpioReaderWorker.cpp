#include "../inc/GpioReaderWorker.h"

GpioReaderWorker::GpioReaderWorker() : Thread(Thread::COOPERATIVE)
{
	
}

GpioReaderWorker::~GpioReaderWorker()
{
	
}

void GpioReaderWorker::init(const char* gpioPath, std::weak_ptr<ReadWriteLock> rwLock, 
					std::weak_ptr<ThreadExecutionData> threadExecutionData)
{
	_gpioPath = gpioPath;
	_rwLock = rwLock;
	_threadExecutionData = threadExecutionData;
}

void* GpioReaderWorker::threadRun()
{
	std::shared_ptr<ThreadExecutionData> threadExecutionData = _threadExecutionData.lock();
	threadExecutionData.requestEvent.waitForSignal(threadExecutionData.requestCondition);
	
	if (threadExecutionData.taskPrompt == posixthreads::ThreadTaskPrompt::RUN_TASK)
	{
		_read(gpio::getCommandStr(gpioCommand));
	}
	else if (threadExecutionData.taskPrompt == posixthreads::ThreadTaskPrompt::WAIT_FOR_SIGNAL)
	{
		threadRun();
	}
	
	return EXIT_SUCCESS;
}

void GpioReaderWorker::readGpio(gpio::GpioCommand gpioCommand)
{
	std::shared_ptr<ThreadExecutionData> threadExecutionData = _threadExecutionData.lock();
	threadExecutionData.threadDataReady.waitForSignal(threadExecutionData.);
	return _read(gpio::getCommandStr(gpioCommand));
}

std::string GpioReaderWorker::_read(const char* gpioCmd) const
{
	std::ifstream fs;
	char* cmdBuffer = new char[gpio::GpioDefines::GPIO_COMMAND_BUFFER_LEN];
	
	strcpy(cmdBuffer, _gpioPath.c_str());
	strcat(cmdBuffer, gpioCmd);
	
	fs.open(cmdBuffer);
	
	if(!fs.is_open())
	{
		std::cerr << "GPIO: read failed to open file: " << gpioCmd << std::endl;
	}
	
	string input;
	getline(fs, input);
	fs.close();
	
	return input;
}
