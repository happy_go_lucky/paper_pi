#include "../inc/GpioManager.h"

GpioManager::GpioManager() noexcept
{
	std::cout << "GpioManager::GpioManager()" << std::endl;
	_gpioRWLockTable = new GpioRWLockLookupTable();
}

GpioManager::~GpioManager()
{
	std::cout << "GpioManager::~GpioManager()" << std::endl;
	delete _gpioRWLockTable;
}

std::unique_ptr<GpioPin> GpioManager::reservePinByGpio(gpio::GpioNumber gpioNumber)
{
	return std::unique_ptr<GpioPin> gpioPin(
				new GpioPin(gpioNumber, _gpioRWLockTable->getReferenceToLock(gpioNumber)), 
				[](GpioPin* p) { delete p; } 
				);
}

std::unique_ptr<GpioPin> GpioManager::reservePin(gpio::PinNumber pinNumber)
{
	// make_shared/unique has issues defining straightforward friendship with smart pointers
	// hence use custom constructor and destructor
	return std::unique_ptr<GpioPin> gpioPin(
				new GpioPin(pinNumber, _gpioRWLockTable->getReferenceToLock(pinNumber)), 
				[](GpioPin* p) { delete p; }
				);
}
