#include <iostream>
#include <unistd.h>  // for the microsecond sleep function
#include <memory>
#include "../inc/LED.h"
#include "../inc/GpioDefines.h"
#include "../inc/GpioManager.h"

using namespace std;

static const int FLASH_DELAY = 50'000; // 50 milliseconds

int main(int argC, char* argV[])
{
   cout << "Starting the makeLEDs program" << endl;
   /**
    * create a shared pointer because this object will be used to store a global
    * reference. It's done to remove the singleton pattern from the codebase.
    * This reference will be passed on using a weak pointer so that the ownership
    * is not shared.
    * */
   GpioManager* gpioManager = new GpioManager();
   
   LED led1(gpio::GpioNumber::GPIO_20, gpioManager);
   LED led2(gpio::GpioNumber::GPIO_21, gpioManager);
   cout << "Flashing the LEDs for 5 seconds" << endl;
   
   led1.turnOn();
   led2.turnOff();
   
   usleep(FLASH_DELAY);
   
   for (int i=0; i<50; i++)
   {
      led1.toggleOnOff();
      led2.toggleOnOff();
      
      usleep(FLASH_DELAY);
   }
   
   cout << "The LED1 state is " << led1.getState() << endl;
   cout << "The LED2 state is " << led2.getState() << endl;
   
   delete gpioManager;
   cout << "Finished the makeLEDs program" << endl;
   
   return EXIT_SUCCESS;
}
