#include "../inc/GpioPin.h"

GpioPin::GpioPin(gpio::GpioNumber gpioPinNumber, std::weak_ptr<ReadWriteLock> rwLock)
{
	_pinNumber = (gpio::PinNumber) gpioPinNumber;
	_rwLock = rwLock;
	_pinPath = string(GPIO_PATH) + string(GPIO_PIN_PREFIX) + 
				to_string(_pinNumber) + string("/");
	
	_readerThread = new GpioReaderWorker();
	_readerThread.init(_rwLock);
	_writerThread.init(_rwLock);
	_exportGPIO();
	usleep(gpio::GpioDefines::GPIO_EXPORT_TIME);	// ensure GPIO is exported
	setMode(gpio::SignalMode::OUTPUT);
}

GpioPin::GpioPin(gpio::PinNumber pinNumber, std::weak_ptr<ReadWriteLock> rwLock)
{
	_pinNumber = pinNumber;
	_rwLock = rwLock;
	_pinPath = string(GPIO_PATH) + string(GPIO_PIN_PREFIX) + 
				to_string(pinNumber) + string("/");
	
	_readerThread.init(_rwLock);
	_writerThread.init(_rwLock);
	_exportGPIO();
	usleep(gpio::GpioDefines::GPIO_EXPORT_TIME);	// ensure GPIO is exported
	setMode(gpio::SignalMode::OUTPUT);
}

GpioPin::~GpioPin()
{
	_unexportGPIO();
}


int GpioPin::setMode(gpio::SignalMode pinMode)
{
	if (pinMode == gpio::SignalMode::INPUT)
		return _write(_pinPath.c_str(), GPIO_MODE_CMD, GPIO_MODE_IN);
	else
		return _write(_pinPath.c_str(), GPIO_MODE_CMD, GPIO_MODE_OUT);
}

gpio::SignalMode GpioPin::getMode() const
{
	if (_read(_pinPath.c_str(), GPIO_MODE_CMD).compare(GPIO_MODE_IN) == 0)
		return gpio::SignalMode::INPUT;
	else
		return gpio::SignalMode::OUTPUT;
}

int GpioPin::setValue(gpio::SignalValue pinValue)
{
	if (pinValue == gpio::SignalValue::HIGH)
		return _write(_pinPath.c_str(), GPIO_VALUE_CMD, VALUE_HIGH_STR);
	else
		return _write(_pinPath.c_str(), GPIO_VALUE_CMD, VALUE_LOW_STR);
}

gpio::SignalValue GpioPin::getValue() const
{
	if (_read(_pinPath.c_str(), GPIO_VALUE_CMD).compare(VALUE_LOW_STR) == 0)
		return gpio::SignalValue::LOW;
	else
		return gpio::SignalValue::HIGH;
}

int GpioPin::_exportGPIO() const
{
	return _write(GPIO_PATH, GPIO_EXPORT_CMD, to_string(_pinNumber).c_str());
}

int GpioPin::_unexportGPIO() const
{
	return _write(GPIO_PATH, GPIO_UNEXPORT_CMD, to_string(_pinNumber).c_str());
}
