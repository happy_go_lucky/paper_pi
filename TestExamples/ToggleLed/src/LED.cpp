#include "../inc/LED.h"

LED::LED(gpio::GpioNumber gpioNumber, const GpioManager* const gpioManager) : _gpioManager(gpioManager)
{
	_gpioPin = _gpioManager->reservePinByGpio(gpioNumber);
	_state = State::OFF;
}

LED::~LED()
{
	cout << "Destroying the LED with GPIO number " << _gpioPin->getPinNumber() << endl;
}

void LED::turnOn()
{
	_gpioPin->setValue(gpio::GpioValue::HIGH);
	_state = State::ON;
}

void LED::turnOff()
{
	_gpioPin->setValue(gpio::GpioValue::LOW);
	_state = State::OFF;
}

LED::State LED::getState() const
{
	if (_gpioPin->getValue() == gpio::GpioValue::HIGH)
	{
		return LED::State::ON;
	}
	return LED::State::OFF;
}

void LED::toggleOnOff()
{
	if (_state == State::ON)
	{
		turnOff();
	}
	else
	{
		turnOn();
	}
}
