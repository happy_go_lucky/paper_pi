#ifndef GPIO_DEFINES_H
#define GPIO_DEFINES_H

namespace gpio
{
	static const short MAX_PINS = 40;
	static const short NUM_INVALID_PINS = 12;
	// an ugly performance optiomization by limiting buffer size
	static const int GPIO_COMMAND_BUFFER_LEN = 100;
	static const int GPIO_EXPORT_TIME = 100'000;
	
	// these commands are representative of files in unix
	// the followiing is the full path 
	// GPIO_PATH + GPIO_PIN_NAME_STR + CMD
	static constexpr const char* GPIO_EXPORT_CMD = "export";
	static constexpr const char* GPIO_UNEXPORT_CMD = "unexport";
	
	// the followiing is the full path 
	// GPIO_PATH + GPIO_PIN_NAME_STR + _pinNumber + OS_DIRECTORY_CHAR + CMD
	static constexpr const char* GPIO_MODE_CMD = "direction";
	static constexpr const char* GPIO_VALUE_CMD = "value";
	static constexpr const char* GPIO_EDGE_CMD = "edge";
	static constexpr const char* GPIO_ACTIVE_LOW_CMD = "active_low";
	
	static constexpr const char* GPIO_MODE_IN = "in";
	static constexpr const char* GPIO_MODE_OUT = "out";
	static constexpr const char* GPIO_EDGE_NONE = "none";
	static constexpr const char* GPIO_EDGE_RISING = "rising";
	static constexpr const char* GPIO_EDGE_FALLING = "falling";
	static constexpr const char* GPIO_EDGE_BOTH = "both";
	
	static constexpr const char* VALUE_LOW_STR = "0";
	static constexpr const char* VALUE_HIGH_STR = "1";
	
	enum InvalidPins
	{
		PIN_1 = 1,		// 3.3V
		PIN_2 = 2,		// 5V
		PIN_4 = 4,		// 5V
		PIN_6 = 6,		// GND
		PIN_9 = 9,		// GND
		PIN_14 = 14,	// GND
		PIN_17 = 17,	// 3.3V
		PIN_20 = 20,	// GND
		PIN_25 = 25,	// GND
		PIN_30 = 30,	// GND
		PIN_34 = 34,	// GND
		PIN_39 = 39		// GND
	};

	enum PinNumber
	{
		PIN_INVALID = 0,
		//~ PIN_1,	// 3.3V
		//~ PIN_2,	// 5V
		PIN_3 = 3,
		//~ PIN_4,	// 5V
		PIN_5 = 5,
		//~ PIN_6,	// GND
		PIN_7 = 7,
		PIN_8,
		//~ PIN_9,	// GND
		PIN_10 = 10,
		PIN_11,
		PIN_12,
		PIN_13,
		//~ PIN_14,	// GND
		PIN_15 = 15,
		PIN_16,
		//~ PIN_17,	// 3.3V
		PIN_18 = 18,
		PIN_19,
		//~ PIN_20,	// GND
		PIN_21 = 21,
		PIN_22,
		PIN_23,
		PIN_24,
		//~ PIN_25,	// GND
		PIN_26 = 26,
		PIN_27,
		PIN_28,	// ID_SC
		PIN_29,	// ID_SD
		//~ PIN_30,	// GND
		PIN_31 = 31,
		PIN_32,
		PIN_33,
		//~ PIN_34,	// GND
		PIN_35 = 35,
		PIN_36,
		PIN_37,
		PIN_38,
		//~ PIN_39,	// GND
		PIN_40 = 40
	};

	enum GpioNumber
	{
		GPIO_INVALID = 0,
		GPIO_2 = PinNumber::PIN_3,	// SDA
		GPIO_3 = PinNumber::PIN_5,	// SCL
		GPIO_4 = PinNumber::PIN_7,	// GPCLK0
		GPIO_5 = PinNumber::PIN_29,
		GPIO_6 = PinNumber::PIN_31,
		GPIO_7 = PinNumber::PIN_26,	// SPIO CE1
		GPIO_8 = PinNumber::PIN_24,	// SPIO CE0
		GPIO_9 = PinNumber::PIN_21,	// SPIO MISO
		GPIO_10 = PinNumber::PIN_19,	// SPIO MOSI
		GPIO_11 = PinNumber::PIN_23,	// SPIO SCLK
		GPIO_12 = PinNumber::PIN_32,	// PWM0
		GPIO_13 = PinNumber::PIN_33,	// PWM1
		GPIO_14 = PinNumber::PIN_8,	// TXD
		GPIO_15 = PinNumber::PIN_10,	// RXD
		GPIO_16 = PinNumber::PIN_36,	// SPI1 CE2
		GPIO_17 = PinNumber::PIN_11,	// SPI1 CE1
		GPIO_18 = PinNumber::PIN_12,	// SPI1 CE0
		GPIO_19 = PinNumber::PIN_35,	// SPI1 MISO
		GPIO_20 = PinNumber::PIN_38,	// SPI1 MOSI
		GPIO_21 = PinNumber::PIN_40,	// SPI1 SCLK
		GPIO_22 = PinNumber::PIN_15,
		GPIO_23 = PinNumber::PIN_16,
		GPIO_24 = PinNumber::PIN_18,
		GPIO_26 = PinNumber::PIN_37,
		GPIO_27 = PinNumber::PIN_13
	};
	
	const InvalidPins ARR_INVALID_PINS[] = 
	{
		InvalidPins::PIN_1,
		InvalidPins::PIN_2,
		InvalidPins::PIN_4,
		InvalidPins::PIN_6,
		InvalidPins::PIN_9,
		InvalidPins::PIN_14,
		InvalidPins::PIN_17,
		InvalidPins::PIN_20,
		InvalidPins::PIN_25,
		InvalidPins::PIN_30,
		InvalidPins::PIN_34,
		InvalidPins::PIN_39
	};

	enum class SignalMode : unsigned char
	{
		INPUT = 0,
		OUTPUT
	};
	
	enum class SignalEdge : unsigned char
	{
		RISING,
		FALLING,
		BOTH,
		NONE
	};
	
	enum class SignalValue : unsigned char
	{
		LOW = 0,
		HIGH
	};
	
	enum class GpioCommand : unsigned char
	{
		EXPORT_PIN,
		UNEXPORT_PIN,
		SIGNAL_MODE,
		SIGNAL_EDGE,
		SIGNAL_VALUE,
		ACTIVE_LOW
	};

	inline char* getCommandStr(GpioCommand command)
	{
		switch (command)
		{
			case GpioCommand::EXPORT_PIN:
			return GPIO_EXPORT_CMD;
			
			case GpioCommand::UNEXPORT_PIN:
			return GPIO_UNEXPORT_CMD;
			
			case GpioCommand::SIGNAL_MODE:
			return GPIO_MODE_CMD;
			
			case GpioCommand::SIGNAL_EDGE:
			return GPIO_EDGE_CMD;
			
			case GpioCommand::SIGNAL_VALUE:
			return GPIO_VALUE_CMD;
			
			case GpioCommand::ACTIVE_LOW:
			return GPIO_ACTIVE_LOW_CMD;
			
			default:
			return nullptr;
		}
	}

	inline char* getSignalValueStr(SignalValue value)
	{
		switch(value)
		{
			case SignalValue::LOW:
			return VALUE_LOW_STR;
			
			case SignalValue::HIGH:
			return VALUE_HIGH_STR;
			
			default:
			return nullptr;
		}
	}

	inline char* getSignalModeStr(SignalMode value)
	{
		switch (value)
		{
			case SignalMode::IN:
			return GPIO_MODE_IN;
			
			case SignalMode::OUT:
			return GPIO_MODE_OUT;
			
			default:
			return nullptr;
		}
	}

	inline char* getSignalEdgeStr(SignalEdge value)
	{
		switch (value)
		{
			case SignalEdge::RISING:
			return GPIO_EDGE_RISING;
			
			case SignalEdge::FALLING:
			return GPIO_EDGE_FALLING;
			
			case SignalEdge::BOTH:
			return GPIO_EDGE_BOTH;
			
			case SignalEdge::NONE:
			default:
			return GPIO_EDGE_NONE;
		}
	}
	
	inline bool isValidPinNumber(PinNumber pinNumber) noexcept
	{
		if (pinNumber < PinNumber::PIN_3 || pinNumber > PinNumber::PIN_40)
		{
			return false;
		}
		
		for (short ii = 0; ii < NUM_INVALID_PINS; ii++)
		{
			if (pinNumber == (PinNumber) ARR_INVALID_PINS[ii])
			{
				return false;
			}
		}
		
		return true;
	}
	
	inline bool isValidGpioNumber(GpioNumber gpioNumber) noexcept
	{
		return isValidPinNumber((PinNumber) gpioNumber);
	}
	
	inline PinNumber gpioNumberToPinNumber(GpioNumber gpioNumber) noexcept
	{
		if (isValidGpioNumber(gpioNumber))
		{
			return (PinNumber) gpioNumber;
		}
		else
		{
			return PinNumber::PIN_INVALID;
		}
	}
}

#endif
