#ifndef THREAD_EXECUTION_DATA_H
#define THREAD_EXECUTION_DATA_H

#include <iostream>
#include <posixthreads/inc/ThreadDefs.h>
#include <posixthreads/inc/MutexLock.h>
#include <posixthreads/inc/ConditionVariable.h>

struct ThreadExecutionData
{
	ThreadExecutionData() = default;
	~ThreadExecutionData() = default;
	
	ThreadExecutionData(const ThreadExecutionData& data) = delete;
	ThreadExecutionData(ThreadExecutionData&& data) = delete;
	std::ostream& operator =(ostream& out, const ThreadExecutionData& data) = delete;
	std::ostream& operator =(ostream& out, ThreadExecutionData&& data) = delete;
	
	MutexLock eventLock;
	MutexLock updateLock;
	ConditionVariable requestEvent;
	ConditionVariable deliveryEvent;
	posixthreads::ThreadEventCondition requestCondition;
	posixthreads::ThreadEventCondition deliveryCondition;
	posixthreads::ThreadTaskPrompt taskPrompt;
};

#endif
