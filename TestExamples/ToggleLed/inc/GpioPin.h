#ifndef GPIO_PIN_H
#define GPIO_PIN_H

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <memory>
#include <unistd.h>	// for the microsecond sleep function
#include <posixthreads/inc/Thread.h>
#include <posixthreads/inc/ReadWriteLock.h>
#include "GpioDefines.h"
#include "GpioReaderWorker.h"
#include "GpioWriterWorker.h"
#include "GpioCommands.h"

using namespace std;

class GpioPin
{
	friend class GpioManager;
	
	public:
		static constexpr const char* GPIO_PATH = "/sys/class/gpio/";
		static constexpr const char* GPIO_PIN_PREFIX = "gpio";
		
		inline gpio::PinNumber getPinNumber() const noexcept
		{
			return _pinNumber;
		}
		
		GpioPin() = delete;
		GpioPin(GpioPin&& gpioPin) = delete;
		GpioPin& operator =(GpioPin&& gpioPin) = delete;
		GpioPin(const GpioPin& gpioPin) = delete;
		GpioPin& operator =(const GpioPin& gpioPin) = delete;
		
		// General I/O settings
		int setMode(gpio::GpioMode pinMode);
		gpio::GpioMode getMode() const;
		int setValue(gpio::GpioValue pinValue);
		gpio::GpioValue getValue() const;
	
	private:
		explicit GpioPin(gpio::GpioNumber gpioPinNumber, std::weak_ptr<ReadWriteLock> rwLock);
		explicit GpioPin(gpio::PinNumber pinNumber, std::weak_ptr<ReadWriteLock> rwLock);
		~GpioPin();
	
		gpio::PinNumber _pinNumber;
		string _pinPath;
		std::weak_ptr<ReadWriteLock> _rwLock;
		GpioReaderWorker* _readerThread;
		GpioWriterWorker* _writerThread;
		
		int _exportGPIO() const;
		int _unexportGPIO() const;
};

#endif
