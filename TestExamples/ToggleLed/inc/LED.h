#ifndef LED_H
#define LED_H

#include <iostream>
#include <string>
#include <memory>
#include "GpioPin.h"
#include "GpioDefines.h"
#include "GpioManager.h"

using namespace std;

class LED
{
	public:
		enum State
		{
			ON = 0,
			OFF
		};
		
		LED() = delete;
		LED(gpio::GpioNumber gpioNumber, const GpioManager* const gpioManager);
		~LED();
		
		void turnOn();
		void turnOff();
		State getState() const;
		void toggleOnOff();
		
	private:
		const GpioManager* const _gpioManager;
		std::shared_ptr<GpioPin> _gpioPin;
		State _state;
};

#endif
