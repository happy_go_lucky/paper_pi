#ifndef GPIO_WRITER_IMPL_H
#define GPIO_WRITER_IMPL_H

#include <iostream>
#include <fstream>
#include <memory>
#include <posixthreads/inc/Thread.h>
#include <posixthreads/inc/ReadWriteLock.h>
#include "GpioDefines.h"

class GpioWriterWorker : public Thread
{
	public:
		GpioWriterWorker();
		~GpioWriterWorker();
		
		void init(std::weak_ptr<ReadWriteLock> rwLock);
		void* threadRun();
		
	private:
		std::weak_ptr<ReadWriteLock> _rwLock;
		
		int _write(const char* path, const char* gpioCmd, const char* value) const;
};

#endif
