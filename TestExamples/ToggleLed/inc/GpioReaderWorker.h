#ifndef GPIO_READER_IMPL_H
#define GPIO_READER_IMPL_H

#include <iostream>
#include <fstream>
#include <string.h>
#include <posixthreads/inc/Thread.h>
#include <posixthreads/inc/MutexLock.h>
#include <posixthreads/inc/ReadWriteLock.h>
#include <memory>
#include "GpioDefines.h"
#include "ThreadExecutionData.h"

class GpioReaderWorker : public Thread
{
	public:
		GpioReaderWorker() = delete;

		GpioReaderWorker(const char* gpioPath, std::shared_ptr<ReadWriteLock> rwLock) noexcept;
		~GpioReaderWorker();

		void init(std::weak_ptr<ThreadExecutionData> threadExecutionData) noexcept;
		void* threadRun();
		std::string readGpio(gpio::GpioCommand gpioCommand) const;

	private:
		const char* _gpioPath;
		std::shared_ptr<ReadWriteLock> _rwLock;
		std::weak_ptr<ThreadExecutionData> _threadExecutionData;

		std::string _read(const char* gpioCmd) const;
};

#endif
