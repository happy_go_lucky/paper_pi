#notes:
#install the libSDL2-devel package for linux
#make sure to use g++ instead of gcc

ver = c++14

ifneq ($(CXX), g++)
	CXX=g++
endif

project = ToggleLEDs
obj_dir = obj
src_dir = src
bin_dir = bin
inc_dir = inc
dep_dir = dep
lib_dir = lib

#0-3 >
optimize_lvl = 0
# 0: no info, 3 max info.
debug_lvl = 3

c_flags = -O$(optimize_lvl) -pedantic -Wpedantic -Wall \
	-Wextra -pthread -std=$(ver) -g$(debug_lvl) -ggdb$(debug_lvl)

# -L points gcc to look for library files in non-default location
external_libs = -L$(lib_dir)

# -l links with a library
# include the shared/static libraries. -labc loads libabc.a/libabc.so
# the typical location of these libraries is /usr/lib
libs = -lpthread -lrt -lposixthreads

# this path is needed for runtime execution of library file since it's
# present at a non-default location 
libs_run_path_ln = -Wl,-rpath=$(lib_dir)

# this path is used to direct linker to look for libs outside of /usr/lib/
lib_inc_path = -I$(lib_dir)


# -MF $(dep_dir)/$*.d: set the dependency file to depname.d
# MT is target
# MF is the filename where the dependencies are saved to
gen_deps = -MT $(@:.d=.o) -MM -MF $(dep_dir)/$*.d

# specify all the include directories here
vpath %.cpp $(src_dir)
vpath %.h $(inc_dir)
vpath %.d $(dep_dir)

subdirs = $(obj_dir) $(dep_dir) $(bin_dir) $(lib_dir)
#find all makefiles for the other libraries
sub_makefiles := $(shell find $(lib_dir)/ -name "*.mk")

src_files = ToggleLEDs.cpp\
	LED.cpp\
	GpioPin.cpp\
	GpioManager.cpp\
	GpioReaderWorker.cpp\
	GpioWriterWorker.cpp\
	GpioRWLockLookupTable.cpp

obj_files = $(src_files:.cpp=.o)
dep_files = $(src_files:.cpp=.d)
obj_file_loc = $(addprefix $(obj_dir)/,$(obj_files))
dep_file_loc = $(addprefix $(dep_dir)/,$(dep_files))

all: $(project)

$(project): $(obj_files) | setup libraries
	$(CXX) $(obj_file_loc) $(libs) $(external_libs) $(libs_run_path_ln) -o $(bin_dir)/$@

%.o: %.cpp
	$(CXX) $(c_flags) $(lib_inc_path) -c $< -o $(obj_dir)/$@

%.d: %.cpp
	$(CXX) $(c_flags) $(exteral_headers) $< $(gen_deps)

#include must not precede the default target
include $(dep_files)

#limits the rule just the contents of variable
$(sub_makefiles): %.mk: dummytarget
	#@echo directory $(@D) and target file $(@F)
	@$(MAKE) -C $(@D) -f $(@F)

.PHONY: setup
.PHONY: clean
.PHONY: all
.PHONY: disassemble
.PHONY: cleanlibs
.PHONY: cleanall
.PHONY: freshbuild
.PHONY: debug
.PHONY: libraries
.PHONY: help
.PHONY: dummytarget

setup:
	@mkdir -p $(obj_dir)
	@mkdir -p $(dep_dir)
	@mkdir -p $(bin_dir)
	@mkdir -p $(lib_dir)

clean:
	@rm -f $(obj_dir)/*.o
	@rm -f $(dep_dir)/*.d
	@rm -f $(bin_dir)/$(project)
	@echo cleaned all temporary files
	
disassemble:
	@objdump -d $(obj_dir)/flashLED.o > obj.dmp

libraries: $(sub_makefiles)

#phony target used for preqrequisistes 
dummytarget:

cleanlibs:
	@$(MAKE) -C $(sub_makefiles) clean

#the | makes the calling ordered if necessary during parallel build (-j)
# prerequisites on the left of the pipe are run normally
# prerequisites on the right of the pipe are run serially and run before left
cleanall: | clean cleanlibs

freshbuild: | cleanall all

debug:
	@echo $(sub_makefiles)

help:
	@$(MAKE) --print-data-base
