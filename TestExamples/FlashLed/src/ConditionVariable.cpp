#include "../inc/ConditionVariable.h"

ConditionVariable::ConditionVariable() noexcept
{
	
}

ConditionVariable::~ConditionVariable()
{
	
}

void ConditionVariable::initialize()
{
	int state = pthread_cond_init(&_cv, NULL);
	if (state != EXIT_SUCCESS)
	{
		std::cout << strerror(state) << std::endl;
		throw ThreadException((char*) "unable to initialize condition variable");
	}
}

void ConditionVariable::cleanup()
{
	int state = pthread_cond_destroy(&_cv);
	if (state != EXIT_SUCCESS)
	{
		std::cout << strerror(state) << std::endl;
		throw ThreadException((char*) "unable to destroy condition variable");
	}
}

/**
 * unblocks one of the waiting threads on the condition variable
 * */
void ConditionVariable::signal()
{
	int state = pthread_cond_signal(&_cv);
	if (state != EXIT_SUCCESS)
	{
		std::cout << strerror(state) << std::endl;
		throw ThreadException((char*) "unable to signal the thread waiting on condition variable");
	}
}

/**
 * if more than one threads are being blocked on the condition variable
 * this will unblock all of them
 * */
void ConditionVariable::signalAll()
{
	int state = pthread_cond_broadcast(&_cv);
	if (state != EXIT_SUCCESS)
	{
		std::cout << strerror(state) << std::endl;
		throw ThreadException((char*) "unable broadcast the threads waiting on condition variable");
	}
}

void ConditionVariable::waitForSignal()
{
	int state = pthread_cond_wait(&_cv, 
	if (state != EXIT_SUCCESS)
	{
		std::cout << strerror(state) << std::endl;
		throw ThreadException((char*) "unable to wait on a condition variable");
	}
}
