#include "../inc/MutexLock.h"

MutexLock::MutexLock() : ISharedLock()
{
	initResources();
}

MutexLock::~MutexLock()
{
	cleanupResources();
}

void MutexLock::initResources()
{
	pthread_mutex_init(&_mutex, NULL);
}

void MutexLock::lock()
{
	pthread_mutex_lock(&_mutex);
}

void MutexLock::unlock()
{
	pthread_mutex_unlock(&_mutex);
}

void MutexLock::cleanupResources()
{
	pthread_mutex_destroy(&_mutex);
}
