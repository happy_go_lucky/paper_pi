#include <iostream>
#include <string>
#include <unistd.h>
#include "../inc/GpioPin.h"

using namespace paperPi;
using std::string;
using std::cout;
using std::endl;

int main()
{
	GpioPin outPin(17);		// pin 11
	GpioPin inPin(27);		// pin 13
	
	outPin.setMode(GpioPin::GpioMode::OUTPUT);
	inPin.setMode(GpioPin::GpioMode::INPUT);
	
	// flash the led 10 times
	for (int ii = 0; ii < 10; ii++)
	{
		outPin.setValue(GpioPin::GpioValue::HIGH);
		usleep(500000);	// 0.5 seconds
		outPin.setValue(GpioPin::GpioValue::LOW);
		usleep(500000);
	}
	
	cout << "The input state is: " << inPin.getValue() << endl;
	
	// fast write example
	outPin.streamOpen();
	for (int ii = 0; ii < 1000000; ii++)	// write 1 million cycles
	{
		outPin.streamWrite(GpioPin::GpioValue::HIGH);
		outPin.streamWrite(GpioPin::GpioValue::LOW);
	}
	outPin.streamClose();
	
	return EXIT_SUCCESS;
}
