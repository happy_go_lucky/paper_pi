#include "../inc/GpioPin.h"

namespace paperPi
{		
	GpioPin::GpioPin(int pinNumber) : Thread(Thread::COOPERATIVE, PTHREAD_STACK_MIN)
	{
		_pinNumber = pinNumber;
		_debounceTime = DEFAULT_DEBOUNCE_TIME;
		_togglePeriod = DEFAULT_TOGGLE_PERIOD;
		_toggleNumber = TOGGLE_NUMBER_INFINITE;
		_callbackFunc = NULL;
		_isThreadRunning = false;
		
		_pinName = GPIO_PIN_NAME_STR + _pinNumber;
		_pinPath = GPIO_PATH + _pinName + OS_DIRECTORY_CHAR;
		_exportGPIO();
		
		// OS needs some time to setup the sysfs
		usleep(OS_DELAY_MICRO_SEC);
	}
	
	GpioPin::~GpioPin()
	{
		_unexportGPIO();
	}
	
	// General I/O settings
	int GpioPin::setMode(GpioMode pinMode)
	{
		switch (pinMode)
		{
			case GpioMode::INPUT:
				return _write(_pinPath, GPIO_MODE_CMD, GPIO_MODE_IN);
			case GpioMode::OUTPUT:
				return _write(_pinPath, GPIO_MODE_CMD, GPIO_MODE_OUT);
			default:
				return INVALID_VALUE;
		}
	}
	
	GpioPin::GpioMode GpioPin::getMode()
	{
		if (_read(_pinPath, GPIO_MODE_CMD).compare(GPIO_MODE_IN) == 0)
			return GpioMode::INPUT;
		else
			return GpioMode::OUTPUT;
	}
	
	int GpioPin::setValue(GpioValue pinValue)
	{
		switch (pinValue)
		{
			case GpioValue::HIGH:
				return _write(_pinPath, GPIO_VALUE_CMD, VALUE_HIGH_INT);
			case GpioValue::LOW:
				return _write(_pinPath, GPIO_VALUE_CMD, VALUE_LOW_INT);
			default:
				return INVALID_VALUE;
		}
	}
	
	GpioPin::GpioValue GpioPin::getValue()
	{
		if (_read(_pinPath, GPIO_VALUE_CMD).compare(VALUE_LOW_STR) == 0)
			return GpioValue::LOW;
		else
			return GpioValue::HIGH;
	}
	
	int GpioPin::setActiveLow()
	{
		return _write(_pinPath, GPIO_ACTIVE_LOW_CMD, VALUE_HIGH_INT);
	}
	
	int GpioPin::setActiveHigh()
	{
		return _write(_pinPath, GPIO_ACTIVE_LOW_CMD, VALUE_LOW_INT);
	}
	
	void GpioPin::setDebounceTime(int time)
	{
		_debounceTime = time;
	}
	
	// advanced output: faster by keeping the stream open ~20 times faster
	int GpioPin::streamOpen()
	{
		_stream.open((_pinPath + GPIO_VALUE_CMD).c_str());
		return EXIT_SUCCESS;
	}
	
	int GpioPin::streamWrite(GpioValue pinValue)
	{
		_stream << pinValue << flush;
		return EXIT_SUCCESS;
	}
	
	int GpioPin::streamClose()
	{
		_stream.close();
		return EXIT_SUCCESS;
	}
	
	int GpioPin::toggleOutput()
	{
		setMode(GpioMode::OUTPUT);
		if (getValue() == GpioValue::HIGH)
			setValue(GpioValue::LOW);
		else
			setValue(GpioValue::HIGH);
		return EXIT_SUCCESS;
	}
	
	int GpioPin::toggleOutput(int time)
	{
		return toggleOutput(-1, time);
	}
	
	int GpioPin::toggleOutput(int numberOfTimes, int time)
	{
		setMode(GpioMode::OUTPUT);
		_toggleNumber = numberOfTimes;
		_togglePeriod = time;
		_isThreadRunning = true;
		
		int threadStatus = pthread_create(&_thread, 
										NULL,
										&_threadedToggle,
										static_cast<void*>(this));
		
		if (threadStatus)
		{
			cerr << "GpioPin: failed to create the toggle thread" << endl;
			_isThreadRunlning = false;
			return INVALID_VALUE;
		}
		
		return EXIT_SUCCESS;
	}
	
	void GpioPin::changeToggleTime(int time)
	{
		_togglePeriod = time;
	}
	
	void GpioPin::cancelToggle()
	{
		_isThreadRunning = false;
	}
	
	// advanced input
	int GpioPin::setEdgeType(GpioEdge gpioEdge)
	{
		switch (gpioEdge)
		{
			case GpioEdge::NONE:
				return _write(_pinPath, GPIO_EDGE_CMD, GPIO_EDGE_NONE);
			case GpioEdge::RISING:
				return _write(_pinPath, GPIO_EDGE_CMD, GPIO_EDGE_RISING);
			case GpioEdge::FALLING:
				return _write(_pinPath, GPIO_EDGE_CMD, GPIO_EDGE_FALLING);
			case GpioEdge::BOTH:
				return _write(_pinPath, GPIO_EDGE_CMD, GPIO_EDGE_BOTH);
			default:
				return INVALID_VALUE;
		}
	}
	
	GpioPin::GpioEdge GpioPin::getEdgeType()
	{
		string edgeValue = _read(_pinPath, GPIO_MODE_CMD);
		if (edgeValue.compare(GPIO_EDGE_RISING) == 0)
			return GpioEdge::RISING;
		else if (edgeValue.compare(GPIO_EDGE_FALLING) == 0)
			return GpioEdge::FALLING;
		else if (edgeValue.compare(GPIO_EDGE_BOTH) == 0)
			return GpioEdge::BOTH;
		else
			return GpioEdge::NONE;
	}
	
	int GpioPin::waitForEdge()
	{
		setMode(GpioMode::INPUT);	// must be an input pin to poll value
		int fd, i, ePollFd, count = 0;
		struct epoll_event ev;
		ePollFd = epoll_create(1);
		
		if (ePollFd == -1)
		{
			perror("GpioPin: Failed to create ePollFd");
			return INVALID_VALUE;
		}
		
		if (fd.open((_pinPath + GPIO_VALUE_CMD).c_str()), O_RDONLY | O_NONBLOCK) == -1)
		{
			perror("GpioPin: Failed to open file");
			return INVALID_VALUE;
		}
		
		// read operation | edge triggered | urgent data
		ev.events = EPOLLIN | EPOLLET | EPOLLPRI;
		
		// attach the file to file descritor
		ev.data.fd = fd;
		
		// register the file descriptor on the epoll instance. see: man epoll_ctl
		if (epoll_ctl(ePollFd, EPOLL_CTL_ADD, fd, &ev) == -1)
		{
			perror("GpioPin: Failed to add control interface");
			return INVALID_VALUE;
		}
		
		while (count <= 1)
		{
			// ignore the trigger
			i = epoll_wait(ePollFd, &ev, 1, -1);
			if (i == -1)
			{
				perror("GpioPin: Poll wait fail");
				count = 5;
			}
			else
			{
				count++;	// count triggers up
			}
		}
		
		close(fd);
		if (count == 5)
			return INVALID_VALUE;
		else
			return EXIT_SUCCESS;
	}	// waits until the button is pressed
	
	int GpioPin::waitForEdge(CallbackType callback)
	{
		_isThreadRunning = true;
		_callbackFunc = callback;
		// create the thread and pass the reference address of the function and data
		if (pthread_create(&this->_thread, NULL, &_threadedPoll, static_cast<void*>(this)))
		{
			perror("GpioPin: Failed to create the poll thread");
			this->_isThreadRunning = false;
			return INVALID_VALUE;
		}
		return EXIT_SUCCESS;
	}	// threaded callback
	
	void GpioPin::cancelEdgeWait()
	{
		_isThreadRunning = false;
	}

	int GpioPin::_write(const string& path, const string& filename, const string& value)
	{
		ofstream fs;
		fs.open((path + filename).c_str());
		
		if (!fs.is_open())
		{
			cerr << "GPIO: write failed to open file: " << filename << endl;
			return INVALID_VALUE;
		}
		fs << value;
		fs.close();
		
		return EXIT_SUCCESS;
	}
	
	int GpioPin::_wrtie(const string& path, const &string filename, int value)
	{
		return _write(path, filename, to_string(value));
	}
	
	string GpioPin::_read(const string& path, const string& filename)
	{
		ifstream fs;
		fs.open((path + filename).c_str());
		
		if(!fs.is_open())
		{
			cerr << "GPIO: read failed to open file: " << filename << endl;
		}
		
		string input;
		getline(fs, input);
		fs.close();
		
		return input;
	}
	
	int GpioPin::_exportGPIO()
	{
		return _write(GPIO_PATH, GPIO_EXPORT_CMD, _pinNumber);
	}
	
	int GpioPin::_unexportGPIO()
	{
		return _write(GPIO_PATH, GPIO_UNEXPORT_CMD, _pinNumber);
	}
	
	void* _threadedPoll(void* value)
	{
		GpioPin* gpioPin = static_cast<GpioPin*>(value);
		while (gpioPin->_isThreadRunning)
		{
			gpioPin->_callbackFunc(gpioPin->waitForEdge());
			usleep(gpioPin->_debounceTime * 1000);
		}
		return EXIT_SUCCESS;
	}
	
	void* _threadedToggle(void* value)
	{
		GpioPin* gpioPin = static_cast<GpioPin*>(value);
		GpioPin::GpioValue pinValue = gpioPin->getValue();
		if (pinValue == GpioPin::GpioValue::HIGH)
		{
			gpioPin->setValue(GpioPin::GpioValue::LOW);
		}
		else
		{
			gpioPin->setValue(GpioPin::GpioValue::HIGH);
		}
		
		usleep(gpioPin->_togglePeriod * GpioPin::TOGGLE_PERIOD_MULTIPLE);	// microseconds
		if (gpioPin->_toggleNumber > 0)
		{
			gpioPin->_toggleNumber--;
		}
		if (gpioPin->_toggleNumber == 0)
		{
			gpioPin->_isThreadRunning = false;
		}
		
		return EXIT_SUCCESS;
	}
}
