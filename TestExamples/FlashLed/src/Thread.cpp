#include "../inc/Thread.h"

Thread::Thread(DetachType detachType, int size)
{
	std::cout << "Thread::Thread()" << std::endl;
	
	_detachType = detachType;
	
	int attirbInitState = pthread_attr_init(&_attributes);
	
	if (attirbInitState != EXIT_SUCCESS)
	{
		std::cout << strerror(attirbInitState) << std::endl;
		throw ThreadException((char*) "unable to initilize attributes for thread");
	}
	
	int detachState;
	
	if (_detachType == DetachType::INDEPENDENT)
	{
		detachState = pthread_attr_setdetachstate(&_attributes, PTHREAD_CREATE_DETACHED);
	}
	else
	{
		detachState = pthread_attr_setdetachstate(&_attributes, PTHREAD_CREATE_JOINABLE);
	}
	
	if (detachState != EXIT_SUCCESS)
	{
		std::cout << strerror(detachState) << std::endl;
		throw ThreadException((char*) "unable to set the detach state for thread");
	}
	
	if (size >= PTHREAD_STACK_MIN)
	{
		int stackSizeState = pthread_attr_setstacksize(&_attributes, size);
		if (stackSizeState != EXIT_SUCCESS)
		{
			std::cout << strerror(stackSizeState) << std::endl;
			throw ThreadException((char*) "unable to set the stack size for thread");
		}
	}
}
		
Thread::~Thread()
{
	std::cout << "Thread::~Thread()" << std::endl;
	
	int state = pthread_attr_destroy(&_attributes);
	
	if (state != EXIT_SUCCESS)
	{
		std::cout << strerror(state) << std::endl;
	}
}

void Thread::start(void* arg)
{
	if (!_isStarted)
	{
		_isStarted = true;		
		args = arg;
		int state = pthread_create(&_id, NULL, &Thread::execute, this);
		
		if (state != EXIT_SUCCESS)
		{
			std::cout << strerror(state) << std::endl;
			throw ThreadException((char*) "unable to create the thread");
		}
	}
}

void Thread::join()
{
	int state = pthread_join(_id, NULL);
	if (state != EXIT_SUCCESS)
	{
		std::cout << strerror(state) << std::endl;
		throw ThreadException((char*) "unable to join the thread with main thread");
	}
}

void Thread::requestCancel()
{
	int state = pthread_cancel(_id);
	if (state != EXIT_SUCCESS)
	{
		std::cout << strerror(state) << std::endl;
		throw ThreadException((char*) "unable to cancel the thread");
	}
}

void* Thread::execute(void* thr)
{
	reinterpret_cast<Thread* >(thr)->run();
}
