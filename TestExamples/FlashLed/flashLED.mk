#notes:
#install the libSDL2-devel package for linux
#make sure to use g++ instead of gcc

ver = c++14

ifneq ($(CXX), g++)
	CXX=g++
endif

project = FlashLed
obj_dir = obj
src_dir = src
bin_dir = bin
inc_dir = inc
dep_dir = dep
lib_dir = lib

#0-3 >
optimize_lvl = 0
# 0: no info, 3 max info.
debug_lvl = 3

c_flags = -O$(optimize_lvl) -pedantic -Wpedantic -Wall \
	-Wextra -pthread -std=$(ver) -g$(debug_lvl) -ggdb$(debug_lvl)

# -L points gcc to look for library files in non-default location
external_libs = -L$(lib_dir)/

# -l links with a library
# include the shared/static libraries. -labc loads libabc.a/libabc.so
# the typical location of these libraries is /usr/lib
libs = -lpthread

# -MF $(dep_dir)/$*.d: set the dependency file to depname.d
# MT is target
# MF is the filename where the dependencies are saved to
gen_deps = -MT $(@:.d=.o) -MM -MF $(dep_dir)/$*.d

# specify all the include directories here
vpath %.cpp $(src_dir)
vpath %.h $(inc_dir)
vpath %.d $(dep_dir)

subdirs = $(obj_dir) $(dep_dir) $(bin_dir) $(lib_dir)

src_files = flashLED.cpp\
	GpioPin.cpp\
	Thread.h\
	MutexLock.h\
	ThreadException.h

obj_files = $(src_files:.cpp=.o)
dep_files = $(src_files:.cpp=.d)
obj_file_loc = $(addprefix $(obj_dir)/,$(obj_files))
dep_file_loc = $(addprefix $(dep_dir)/,$(dep_files))

all: $(project)

$(project): $(obj_files) | setup
	$(CXX) $(obj_file_loc) $(libs) $(external_libs) -o $(bin_dir)/$@

%.o: %.cpp
	$(CXX) $(c_flags) $(exteral_headers) -c $< -o $(obj_dir)/$@

%.d: %.cpp
	$(CXX) $(c_flags) $(exteral_headers) $< $(gen_deps)

#include must not precede the default target
include $(dep_files)

setup:
	@mkdir -p $(obj_dir)
	@mkdir -p $(dep_dir)
	@mkdir -p $(bin_dir)
	@mkdir -p $(lib_dir)

.PHONY: setup
.PHONY: clean
.PHONY: all
.PHONY: disassemble

clean:
	@rm -f $(obj_dir)/*.o
	@rm -f $(dep_dir)/*.d
	@rm -f $(bin_dir)/$(project)
	@echo cleaned all temporary files
	
disassemble:
	@objdump -d $(obj_dir)/flashLED.o > obj.dmp
