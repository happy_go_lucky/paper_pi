#ifndef CONDITION_VARIABLE_H
#define CONDITION_VARIABLE_H

#include <pthread.h>
#include <string.h>
#include "ThreadException.h"

namespace paperPi
{
	class ConditionVariable
	{
		public:
			ConditionVariable() noexcept;
			ConditionVariable(const ConditionVariable& cv) = delete;
			ConditionVariable(ConditionVariable&& cv) = delete;
			~ConditionVariable();
			
			ConditionVariable& operator =(const ConditionVariable& cv) = delete;
			ConditionVariable& operator =(ConditionVariable&& cv) = delete;
			
			void initialize();
			void cleanup();
			void signal();
			void signalAll();
			void waitForSignal();
		
		private:
			pthread_cond_t _cv;
	};
}

#endif
