#ifndef THREAD_EXCEPTION_H
#define THREAD_EXCEPTION_H

#include <exception>

using std::exception;

namespace paperPi
{
	class ThreadException : public exception
	{
		public:
			ThreadException( char* = (char*) "Exception!" );
			virtual const char* what() const throw();
			
		private:
			char* _message;
	};
}

#endif
