#ifndef GPIO_PIN_H
#define GPIO_PIN_H

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <sstream>
#include <cstdlib>
#include <cstdio>
#include <fcntl.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <pthread.h>
#include "Thread.h"

using namespace std;

//~ #define GPIO_PATH "/sys/class/gpio/"

namespace paperPi
{
	class GpioPin : public Thread
	{
		public:
			typedef int (*CallbackType)(int);
			
			static constexpr const char* GPIO_PATH = "/sys/class/gpio/";
			static constexpr const char* GPIO_PIN_NAME_STR = "gpio";
			static constexpr const char* OS_DIRECTORY_CHAR = "/";
			
			// these commands are representative of files in unix
			// the followiing is the full path 
			// GPIO_PATH + GPIO_PIN_NAME_STR + CMD
			static constexpr const char* GPIO_EXPORT_CMD = "export";
			static constexpr const char* GPIO_UNEXPORT_CMD = "unexport";
			// the followiing is the full path 
			// GPIO_PATH + GPIO_PIN_NAME_STR + _pinNumber + OS_DIRECTORY_CHAR + CMD
			static constexpr const char* GPIO_MODE_CMD = "direction";
			static constexpr const char* GPIO_VALUE_CMD = "value";
			static constexpr const char* GPIO_EDGE_CMD = "edge";
			static constexpr const char* GPIO_ACTIVE_LOW_CMD = "active_low";
			
			static constexpr const char* GPIO_MODE_IN = "in";
			static constexpr const char* GPIO_MODE_OUT = "out";
			static constexpr const char* GPIO_EDGE_NONE = "none";
			static constexpr const char* GPIO_EDGE_RISING = "rising";
			static constexpr const char* GPIO_EDGE_FALLING = "falling";
			static constexpr const char* GPIO_EDGE_BOTH = "both";
			
			static constexpr const char* VALUE_LOW_STR = "0";
			static constexpr const char* VALUE_HIGH_STR = "1";
			
			static const int DEFAULT_DEBOUNCE_TIME = 0;
			static const int TOGGLE_NUMBER_INFINITE = -1;
			static const int OS_DELAY_MICRO_SEC = 250'000;
			static const int INVALID_VALUE = -1;
			
			static const unsigned int VALUE_HIGH_INT = 1;
			static const unsigned int VALUE_LOW_INT = 0;
			static const unsigned int DEFAULT_TOGGLE_PERIOD = 100;
			static const unsigned int TOGGLE_PERIOD_MULTIPLE = 500;
			
			enum GpioMode
			{
				INPUT = 0,
				OUTPUT
			};
			
			enum GpioValue
			{
				LOW = 0,
				HIGH
			};
			
			enum GpioEdge
			{
				NONE = 0,
				RISING,
				FALLING,
				BOTH
			};
		
			GpioPin(int pinNumber);
			~GpioPin();
			
			inline int getPinNumber() const noexcept
			{
				return _pinNumber;
			}
			
			// General I/O settings
			int setMode(GpioMode pinMode);
			GpioMode getMode();
			int setValue(GpioValue pinValue);
			GpioValue getValue();
			int setActiveLow();
			int setActiveHigh();	// should be the default state
			void setDebounceTime(int time);
			
			// advanced output: faster by keeping the stream open ~20 times faster
			int streamOpen();
			int streamWrite(GpioValue pinValue);
			int streamClose();
			int toggleOutput();
			int toggleOutput(int time);
			int toggleOutput(int numberOfTimes, int time);
			void changeToggleTime(int time);
			void cancelToggle();
			
			// advanced input
			int setEdgeType(GpioEdge gpioEdge);
			GpioEdge getEdgeType();
			int waitForEdge();	// waits until the button is pressed
			int waitForEdge(CallbackType callback);	// threaded callback
			void cancelEdgeWait();
			
			friend void* _threadedPoll(void* value);
			friend void* _threadedToggle(void* value);
			
		private:
			int _pinNumber;
			int _debounceTime;
			string _pinName;
			string _pinPath;
			ofstream _stream;
			pthread_t _thread;
			CallbackType _callbackFunc;
			bool _isThreadRunning;
			int _togglePeriod;	// default 100 micro seconds
			int _toggleNumber;	// default -1 (inifinite)
			
			int _write(const string& path, const string& filename, const string& value);
			int _wrtie(const string& path, const string& filename, int value);
			string _read(const string& path, const string& filename);
			int _exportGPIO();
			int _unexportGPIO();
	};
	
}

#endif
