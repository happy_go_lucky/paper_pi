/**
 * this class represents the waveshare epaper 1.54 inch screen module
 * 
 * Pin diagram
 * 
 * BUSY	: purple cable	: busy state (output) pin. busy when high. No commands should be issued
 * RST	: white cable	: reset pin. active on low
 * DC	: green cable	: data/command control pin. data on high, and command on low
 * CS	: orange cable	: SPI slave chip select (input) pin. chip is enabled on low
 * CLK	: yellow cable	: serial clock pin (SPI SCK pin or spi communication pin)
 * DIN	: blue cable	: SPI MOSI pin. data line from master to slave in SPI comm
 * GND	: black cable	: ground pin
 * 3.3V	: red cable		: core logic power pin
 * 
 * Data is transferred 8-bits in one clock cycle. Transferred as MSB first.
 * SPI0 is used.
 * 
 * */
